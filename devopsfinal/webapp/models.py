from django.db import models

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=64)
    username = models.CharField(max_length=16)
    
    def __str__(self):
        return f'{self.username}: {self.last_name}, {self.first_name}'
