from django.urls import resolve
from django.test import TestCase

from webapp.views import home
# Create your tests here.

class HomePageTests(TestCase):
    def test_root_page_resolves_to_home_page(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_page_returns_correct_html(self):
        response = self.client.get('/')
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<!DOCTYPE html>'))
        self.assertIn('<title>SPDR Stock Display</title>', html)
        self.assertTrue(html.strip().endswith('</html>'))

