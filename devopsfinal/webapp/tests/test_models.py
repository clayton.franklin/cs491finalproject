from django.test import TestCase

from webapp.models import User

class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(username='claytonf', first_name='Clayton', last_name='Franklin')

    def test_first_name_label(self):
        user = User.objects.get(id=1)
        field_label = user._meta.get_field('first_name').verbose_name
        self.assertEqual(field_label, 'first name')

    def test_object_name_is_username_colon_last_name_comma_first_name(self):
        user = User.objects.get(id=1)
        expected_object_name = f'{user.username}: {user.last_name}, {user.first_name}'
        self.assertEqual(expected_object_name, str(user))
