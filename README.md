[![pipeline status](https://gitlab.com/clayton.franklin/cs491finalproject/badges/master/pipeline.svg)](https://gitlab.com/clayton.franklin/cs491finalproject/-/commits/master)
# Devops Final Project
Created by:
Clayton Franklin

Config for pipeline:
`.gitlab-ci.yml`

AWS config is a Gitlab Runner that pulls code when the repo is changed

Live demo
